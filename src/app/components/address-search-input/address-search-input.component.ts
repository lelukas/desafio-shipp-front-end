import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-address-search-input',
  templateUrl: './address-search-input.component.html',
  styleUrls: ['./address-search-input.component.sass']
})
export class AddressSearchInputComponent implements OnInit {
  constructor() {}

  @Output() addressChange = new EventEmitter();
  @Input() value: string;

  address = undefined;
  faSearch = faSearch;
  options = [];

  public emitAddress(address: any) {
    this.addressChange.emit(address);
  }

  ngOnInit() {}

}
