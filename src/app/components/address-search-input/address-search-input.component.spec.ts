import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressSearchInputComponent } from './address-search-input.component';

describe('AddressSearchInputComponent', () => {
  let component: AddressSearchInputComponent;
  let fixture: ComponentFixture<AddressSearchInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressSearchInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressSearchInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
