import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmDirectionModule } from 'agm-direction';
import { MapComponent } from './components/map/map.component';
import { NewAddressComponent } from './pages/new-address/new-address.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AddressSearchInputComponent } from './components/address-search-input/address-search-input.component';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { AddressService } from './services/address.service';
import { DeliveryService } from './services/delivery.service';
import { PaymentService } from './services/payment.service';
import { HttpClientModule } from '@angular/common/http';
import { NewCreditCardComponent } from './pages/new-credit-card/new-credit-card.component';
import { OrderCompleteComponent } from './pages/order-complete/order-complete.component';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MapComponent,
    routingComponents,
    NewAddressComponent,
    AddressSearchInputComponent,
    CheckboxComponent,
    NewCreditCardComponent,
    OrderCompleteComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBquT8-yj_IwK5ThQM7mjTK6BBnwea9vTg',
      language: 'pt-BR'
    }),
    GooglePlaceModule,
    AgmDirectionModule,
    FontAwesomeModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot()
  ],
  providers: [
    AddressService,
    DeliveryService,
    PaymentService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
