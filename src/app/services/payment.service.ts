import { Injectable } from '@angular/core';

export interface CreditCard {
  card_number: number;
  expiry_date: number;
  name: string;
  cvv: number;
}

@Injectable()
export class PaymentService {
  creditCards: CreditCard[];

  constructor() {
    this.creditCards = JSON.parse(localStorage.getItem('creditCards')) || [];
  }

  addCreditCard(card: CreditCard) {
    const creditCardArr = [ ...this.creditCards, card ];
    localStorage.setItem('creditCards', JSON.stringify(creditCardArr));
    this.creditCards = creditCardArr;
  }

  removeCreditCard(creditCardIndex: number) {
    const creditCardArr = [ ...this.creditCards ];
    creditCardArr.splice(creditCardIndex, 1);
    localStorage.setItem('creditCards', JSON.stringify(creditCardArr));
    this.creditCards = [ ...creditCardArr ];
  }
}
