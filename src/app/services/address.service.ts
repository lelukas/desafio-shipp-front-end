import {Injectable} from '@angular/core';

@Injectable()
export class AddressService {
  constructor() {

  }
  addresses: any = [];
  waypoints: any = [];
  origin: any;
  destination: any;

  addAddress(addressObject): void {
    if (!this.origin) {
      this.updateOrigin(addressObject);
    } else if (!this.destination) {
      this.updateDestination(addressObject);
    } else {
      const location = { ...this.destination };
      this.destination = {
        lat: addressObject.geometry.location.lat(),
        lng: addressObject.geometry.location.lng()
      };
      this.waypoints.push({ location });
    }
    this.addresses.push(addressObject);
  }

  updateOrigin(address, isEditing?: boolean) {
    this.origin = {
      lat: address.geometry.location.lat(),
      lng: address.geometry.location.lng()
    };
    if (isEditing) {
      this.addresses[0] = address;
    }
  }

  updateDestination(address, isEditing?: boolean) {
    this.destination = {
      lat: address.geometry.location.lat(),
      lng: address.geometry.location.lng()
    };
    if (isEditing) {
      this.addresses[this.addresses.length - 1] = address;
    }
  }

  updateWaypoint(address, addressIndex: number) {
    const waypointIndex = addressIndex - 1;
    this.addresses[addressIndex] = address;
    const location = {
      lat: address.geometry.location.lat(),
      lng: address.geometry.location.lng()
    };
    this.waypoints[waypointIndex] = { location };
    this.forceWaypointsRerender();
  }

  forceWaypointsRerender() {
    // Forces map to update ngm-direction waypoints
    const waypointsArr = [...this.waypoints];
    this.waypoints = null;
    this.waypoints = [...waypointsArr];
  }

  removeStop(addressIndex) {
    if (addressIndex === this.addresses.length - 1 && !!this.destination) {
      // console.log('waypoints length', this.waypoints.length);
      if (this.waypoints.length === 0) {
        this.destination = undefined;
      } else if (this.waypoints.length === 1) {
        this.destination = this.waypoints[0].location;
        this.waypoints = [];
      } else if (this.waypoints.length) {
        this.destination = this.waypoints[this.waypoints.length - 1].location;
        this.waypoints.pop();
      }
      this.addresses.pop();
    } else {
      this.waypoints.splice(addressIndex - 1, 1);
      this.addresses.splice(addressIndex, 1);
    }
    this.forceWaypointsRerender();
  }
}
