# Algumas resalvas importantes

* Para conseguir utilizar a rota `/evaluate`, precisei desativar o CORS do navegador. Só utilizando uma extensão não deu certo, foi necessário desativar nas flags do navegador. Veja esse link aqui https://alfilatov.com/posts/run-chrome-without-cors/
* Já a rota `/service`, não consegui fazer a requisição POST nem com o CORS desativado. O código referente a ela está no projeto, mas está comentado.
* Fiz uma versão ligeiramente diferente do que estava no protótipo. Não consegui compreender o fluxo de navegação em sua totalidade, então tive que assumir algumas coisas e fazer as alterações em cima delas. Estou a disposição para conversar sobre elas.
* Não adicionei logo pois ela não estava marcada pra exportação no protótipo do XD.
* Há versão para mobile e tablet. Não havia um protótipo definindo a interface, mas fiz uma mesmo assim.

---


# ShippAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.18.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
